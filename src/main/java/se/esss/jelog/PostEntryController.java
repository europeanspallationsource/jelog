/*
 * Copyright (C) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package se.esss.jelog;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Pair;

/**
 * FXML Controller class
 *
 * @author nataliamilas
 * @author Juan F. Esteban Müller <juanf.estebanmuller@esss.se>
 */
public class PostEntryController implements Initializable {

    @FXML
    private TextArea textBody;
    @FXML
    private Button buttonCancel;
    @FXML
    private Button buttonSubmit;
    @FXML
    private ComboBox<String> comboBoxLogBook;

    private String elogServer = "https://logbook.esss.lu.se/";

    private String logbookGroup = null;

    private List<Attachment> attachments = new ArrayList<>();

    @FXML
    private GridPane gridPane;
    @FXML
    private ComboBox<String> comboBoxLogBookGroup;

    private ArrayList<Region> gridControls = null;

    private String author = null;

    private String userName = null;

    private String userPasswordHash = null;
    @FXML
    private Button switchUserButton;

    private String defaultLogbook = null;
    @FXML
    private TextField replyToTextField;
    @FXML
    private FlowPane thumbnailsPane;
    @FXML
    private ScrollPane thumbnailsScrollPane;
    @FXML
    private Button addAttachmentButton;

    public void setDefaultLogbook(String defaultLogbook) {
        this.defaultLogbook = defaultLogbook;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserPassword(char[] userPassword) {
        this.userPasswordHash = Sha256.sha256(userPassword, 5000).substring(4);
    }

    public void setUserPasswordHash(String userPasswordHash) {
        this.userPasswordHash = userPasswordHash;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Deprecated
    public void setSnapshots(WritableImage[] snapshots) {
        int i = 0;
        for (WritableImage snapshot : snapshots) {
            attachments.add(new Attachment("screenshot_" + Integer.toString(i) + ".png", snapshot));
            i++;
        }

        updateAttachments();
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;

        updateAttachments();
    }

    @Deprecated
    public void setAttachments(Attachment[] attachments) {
        this.attachments.addAll(Arrays.asList(attachments));

        updateAttachments();
    }

    public void setElogServer(String elogServer) {
        this.elogServer = elogServer.endsWith("/") ? elogServer : elogServer + '/';
        updateLogbook();
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            Jelog.setTrustAllCerts();
        } catch (Exception ex) {
            Logger.getLogger(PostEntryController.class.getName()).log(Level.SEVERE, null, ex);
        }

        gridControls = new ArrayList();

        updateLogbookGroups();
    }

    public boolean login() throws IOException, Exception {
        AuthenticationPaneFX pane = new AuthenticationPaneFX();
        Dialog dlg = pane.createDialog(null);

        String logbook = comboBoxLogBook.getSelectionModel().getSelectedItem();
        String[] credentials = Jelog.retrieveUsernameAndPassword(new URL(new URL(elogServer), logbook).toString());
        while (credentials[0] == null) {
            Optional<Pair<String, char[]>> result = dlg.showAndWait();
            if (result.isPresent()) {
                Jelog.login(result.get().getKey(), result.get().getValue(), true, elogServer);

                credentials = Jelog.retrieveUsernameAndPassword(new URL(new URL(elogServer), logbook).toString());
            } else {
                return false;
            }
        }

        setAuthor(credentials[0]);
        setUserName(credentials[1]);
        setUserPasswordHash(credentials[2]);

        // Updating again to reload with user parameters introduced after the initialisation.
        updateLogbook();

        return true;
    }

    /**
     * Updates the logbook, categories and types lists.
     */
    private void updateLogbookGroups() {
        List<String> logbookgroups = null;
        try {
            logbookgroups = Jelog.getLogbooks(elogServer);
        } catch (IOException ex) {
            Logger.getLogger(PostEntryController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RuntimeException ex) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("The logbook server is not reachable!");
            alert.showAndWait();
        }

        comboBoxLogBookGroup.setItems(FXCollections.observableArrayList(logbookgroups));

        if (logbookgroups.isEmpty()) {
            comboBoxLogBook.setDisable(true);
        } else {
            comboBoxLogBook.setDisable(false);
            comboBoxLogBookGroup.getSelectionModel().selectFirst();
            comboBoxLogBookGroupHandler(null);
        }
    }

    /**
     * Fills in the logbooks available for the selected logbook group.
     *
     * @param event
     */
    @FXML
    private void comboBoxLogBookGroupHandler(ActionEvent event) {
        if (!comboBoxLogBookGroup.getSelectionModel().isEmpty()) {
            logbookGroup = comboBoxLogBookGroup.getSelectionModel().getSelectedItem().replaceAll(" ", "+");

            updateLogbook();
        }
    }

    /**
     * Updates the logbook, categories and types lists.
     */
    private void updateLogbook() {
        List<String> logbooks = null;
        try {
            logbooks = Jelog.getLogbooks(elogServer + logbookGroup);
        } catch (IOException ex) {
            Logger.getLogger(PostEntryController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RuntimeException ex) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("The logbook server is not reachable!");
            alert.showAndWait();
        }

        comboBoxLogBook.setItems(FXCollections.observableArrayList(logbooks));
        if (!logbooks.isEmpty()) {
            if (defaultLogbook != null && logbooks.contains(defaultLogbook)) {
                comboBoxLogBook.getSelectionModel().select(comboBoxLogBook.getItems().indexOf(defaultLogbook));
            } else {
                comboBoxLogBook.getSelectionModel().selectFirst();
            }
            comboBoxLogBookHandler(null);
        }
    }

    private String getCheckedItems(TilePane pane) {
        List<String> checkedItems = new ArrayList<>();

        for (Node checkBox : pane.getChildren()) {
            if (((CheckBox) checkBox).isSelected()) {
                checkedItems.add(((CheckBox) checkBox).getText());
            }
        }

        String items = "";
        for (String item : checkedItems) {
            items = items.concat(item + " | ");
        }

        if (!items.isEmpty()) {
            items = items.substring(0, items.length() - 3);
        }

        return items.trim();
    }

    @FXML
    private void handleButtonSubmit(ActionEvent event) {

        boolean allrequired = true;
        HashMap<String, String> fields = new HashMap();

        for (int i = 4; i < gridPane.getChildren().size(); i += 2) {
            Label label = (Label) gridPane.getChildren().get(i);

            String option = null;

            if (gridPane.getChildren().get(i + 1).getClass() == TextField.class) {
                option = ((TextField) gridPane.getChildren().get(i + 1)).getText();
            } else if (gridPane.getChildren().get(i + 1).getClass() == ComboBox.class) {
                option = ((ComboBox<String>) gridPane.getChildren().get(i + 1)).getSelectionModel().getSelectedItem();
            } else if (gridPane.getChildren().get(i + 1).getClass() == TilePane.class) {
                option = getCheckedItems((TilePane) gridPane.getChildren().get(i + 1));
            }

            fields.put(label.getText(), option);

            if (label.getTextFill() == ((Paint) Color.RED) && option.isEmpty() || textBody.getText().isEmpty()) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("Please fill all the required fields (red) and text before submitting the new entry.");
                alert.showAndWait();
                allrequired = false;
                break;
            }
        }

        if (allrequired) {
            try {
                Jelog.submit(fields, textBody.getText(), null, attachments, comboBoxLogBook.getValue(),
                        elogServer, userName, userPasswordHash);
            } catch (IOException ex) {
                Logger.getLogger(PostEntryController.class.getName()).log(Level.SEVERE, null, ex);
            }

            Stage stage = (Stage) buttonSubmit.getScene().getWindow();
            stage.close();
        }
    }

    /**
     * Fills in the categories and types combo boxes for the selected logbook.
     *
     * @param event
     */
    @FXML
    private void comboBoxLogBookHandler(ActionEvent event) {
        if (!comboBoxLogBook.getSelectionModel().isEmpty()) {
            String logbook = comboBoxLogBook.getSelectionModel().getSelectedItem().replaceAll(" ", "+");

            HashMap<String, LogbookAttribute> attributesMap = Jelog.getLogbookAttributes(elogServer, logbookGroup, logbook);

            cleanGridPane();
            updateGridPane(attributesMap);
        }
    }

    private void addNewTextField(String attribute, List<Label> labels, List<Region> controls) {
        Label label = new Label(attribute);
        TextField textField = new TextField();
        labels.add(label);
        controls.add(textField);
    }

    private void addNewComboBox(String attribute, List<Label> labels, List<Region> controls, String[] items) {
        Label label = new Label(attribute);
        ComboBox comboBox = new ComboBox<>();
        comboBox.getItems().addAll(Arrays.asList(items));
        comboBox.getSelectionModel().selectFirst();
        labels.add(label);
        controls.add(comboBox);
    }

    private void addNewCheckBox(String attribute, List<Label> labels, List<Region> controls, String[] items) {
        Label label = new Label(attribute);
        TilePane tilePane = new TilePane();
        tilePane.setTileAlignment(Pos.CENTER_LEFT);

        for (String item : items) {
            CheckBox checkBox = new CheckBox(item);
            gridControls.add(checkBox);
            tilePane.getChildren().add(checkBox);
        }

        if (tilePane.getChildren().size() == 1) {
            ((CheckBox) tilePane.getChildren().get(0)).setSelected(true);
        }
        labels.add(label);
        controls.add(tilePane);
    }

    /**
     * Updates the grid pane with all fields
     *
     * @param attributesMap
     */
    private void updateGridPane(HashMap<String, LogbookAttribute> attributesMap) {
        List<Label> labels = new ArrayList();
        List<Region> controls = new ArrayList();
        for (String attribute : attributesMap.keySet()) {
            if (attributesMap.get(attribute).getOptions() == null) {
                addNewTextField(attribute, labels, controls);
            } else if (!attributesMap.get(attribute).isMultioption()) {
                addNewComboBox(attribute, labels, controls, attributesMap.get(attribute).getOptions());
            } else if (attributesMap.get(attribute).isMultioption()) {
                addNewCheckBox(attribute, labels, controls, attributesMap.get(attribute).getOptions());
            }
            if (attributesMap.get(attribute).isRequired()) {
                labels.get(labels.size() - 1).setTextFill(Color.RED);
            }

            if (attributesMap.get(attribute).isLocked()) {
                controls.get(controls.size() - 1).setDisable(true);
                if (controls.get(controls.size() - 1).getClass().isInstance(TilePane.class)) {
                    for (Node control : controls.get(controls.size() - 1).getChildrenUnmodifiable()) {
                        control.setDisable(true);
                    }
                }
            }

            if (attribute.equals("Author")) {
                ((TextField) controls.get(controls.size() - 1)).setText(author);
            }

            // Set the default Session ID
            if (attribute.equals("Session ID")) {
                ((TextField) controls.get(controls.size() - 1)).setText(new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime()) + "A");
            }
        }

        // Update gridPane with all new controls
        int initialNumberOfRows = gridPane.getChildren().size() / 2;

        for (int i = 0; i < labels.size(); i++) {
            gridPane.add(labels.get(i), 0, initialNumberOfRows + i);
            gridPane.add(controls.get(i), 1, initialNumberOfRows + i);
            gridControls.add(labels.get(i));
            gridControls.add(controls.get(i));
        }
    }

    // Cleans up the previous controls
    private void cleanGridPane() {
        for (Region control : gridControls) {
            gridPane.getChildren().remove(control);
        }
    }

    @FXML
    private void addAttachmentButtonAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Add attachment");
        File file = fileChooser.showOpenDialog(addAttachmentButton.getScene().getWindow());
        if (file != null) {
            try {
                attachments.add(new Attachment(file));
                updateAttachments();
            } catch (IOException ex) {
                Logger.getLogger(PostEntryController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void updateAttachments() {
        thumbnailsPane.getChildren().clear();
        if (!attachments.isEmpty()) {
            for (Attachment attachment : attachments) {
                StackPane stackPane = createStackPane(attachment);
                thumbnailsPane.getChildren().addAll(stackPane);
            }
        }
    }

    private ImageView createImageView(Image image) {
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(90);
        imageView.setFitWidth(200);
        imageView.setPreserveRatio(true);
        // apply a shadow effect.
        imageView.setEffect(new DropShadow(5, Color.BLACK));

        return imageView;
    }

    private ImageView removeIcon(Attachment attachment) {
        Image removeIconI = new Image("pictures/remove_icon_25x25.png");
        ImageView removeIconIW = new ImageView(removeIconI);
        removeIconIW.setFitHeight(25);
        removeIconIW.setFitWidth(25);
        removeIconIW.setPreserveRatio(true);

        removeIconIW.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 1) {
                        attachments.remove(attachment);
                        updateAttachments();
                    }
                }
            }
        });

        return removeIconIW;
    }

    private StackPane createStackPane(Attachment attachment) {
        Label label = new Label(attachment.getFileName());
        label.setWrapText(true);
        label.setAlignment(Pos.CENTER);
        label.setStyle("-fx-background-color: rgba(255,255,255,0.9);");
        StackPane innerStackPane = new StackPane();

        // Show a preview for images or a square for binary attachments.
        if (attachment.isImage()) {
            ImageView imageView = createImageView(attachment.getImage());
            label.setMaxWidth(0.9 * imageView.getLayoutBounds().getWidth());
            innerStackPane.getChildren().addAll(imageView, label);
        } else {
            Rectangle rect = new Rectangle();
            rect.setHeight(90);
            rect.setWidth(150);
            rect.setFill(Color.WHITE);
            rect.setEffect(new DropShadow(5, Color.BLACK));
            label.setMaxWidth(0.9 * rect.getLayoutBounds().getWidth());
            innerStackPane.getChildren().addAll(rect, label);

        }

        // Adds a remove icon to delete attachments.
        ImageView removeIconIW = removeIcon(attachment);
        StackPane outerStackPane = new StackPane();
        outerStackPane.setAlignment(Pos.TOP_RIGHT);
        outerStackPane.getChildren().addAll(innerStackPane, removeIconIW);

        // Clicking opens a larger preview for images.
        if (attachment.isImage()) {
            innerStackPane.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {

                    if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                        if (mouseEvent.getClickCount() == 1) {
                            BorderPane borderPane = new BorderPane();
                            ImageView imageView = new ImageView();
                            imageView.setImage(attachment.getImage());
                            imageView.setPreserveRatio(true);
                            imageView.setSmooth(true);
                            imageView.setCache(true);
                            borderPane.setCenter(imageView);

                            Stage newStage = new Stage();
                            Scene scene = new Scene(borderPane, Color.BLACK);
                            newStage.setTitle(attachment.getFileName());
                            newStage.initModality(Modality.APPLICATION_MODAL);
                            newStage.setScene(scene);
                            // Preview can be close by clicking on it.
                            imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                @Override
                                public void handle(MouseEvent mouseEvent) {
                                    if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                                        newStage.close();
                                    }
                                }
                            });
                            newStage.show();
                        }
                    }
                }
            });
        }

        return outerStackPane;
    }

    @FXML
    private void handleSwitchUserButton(ActionEvent event) throws Exception {
        Jelog.logout();

        boolean loginResult = login();

        if (!loginResult) {
            handleButtonCancel();
        }
    }

    @FXML
    private void handleButtonCancel() {
        Stage stage = (Stage) buttonCancel.getScene().getWindow();
        stage.close();
    }
}
