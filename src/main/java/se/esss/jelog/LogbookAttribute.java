/*
 * Copyright (C) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package se.esss.jelog;

/**
 *
 * @author Juan F. Esteban Müller <juanf.estebanmuller@esss.se>
 */
public class LogbookAttribute {

        private boolean required;
        private boolean locked;
        private boolean multioption;
        private String[] options;

        public void setLocked(boolean locked) {
            this.locked = locked;
        }

        public boolean isLocked() {
            return locked;
        }

        public void setRequired(boolean required) {
            this.required = required;
        }

        public boolean isRequired() {
            return required;
        }

        public void setMultioption(boolean multioption) {
            this.multioption = multioption;
        }

        public boolean isMultioption() {
            return multioption;
        }

        public void setOptions(String[] options) {
            this.options = options;
        }

        public String[] getOptions() {
            return options;
        }

        LogbookAttribute(boolean required, boolean locked, boolean multioption,
                String[] options) {
            this.locked = locked;
            this.required = required;
            this.multioption = multioption;
            this.options = options;
        }

        public static LogbookAttribute newInstance(boolean required, boolean locked, boolean multioption,
                String[] options) {
            return new LogbookAttribute(required, locked, multioption, options);
        }

        public static LogbookAttribute newEmpty() {
            return new LogbookAttribute(false, false, false, null);
        }
    }
